package com.thayssantos.mathem;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.thayssantos.mathem.model.Delivery;
import com.thayssantos.mathem.model.Product;
import com.thayssantos.mathem.model.Products;
import com.thayssantos.mathem.model.constants.ProductType;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeliveryValidation {
    private static final int DELIVERY_LIMIT = 14;
    private static final int EXTERNAL_PROD_PLUS_DAYS = 5;
    private static int LIMIT_GREEN_DELIVERY_DAY = 3;

    private static DateTime currentDay = new DateTime();
    private static DateTime startDay = currentDay;
    private static DateTime lastDay = currentDay.plusDays(DELIVERY_LIMIT);

    private static List<Product> products = new ArrayList<>();
    private static String postCode = "12345";

    static Map<String, List<DateTime>> greenList;

    private static List<Delivery> deliveryGreenList = new ArrayList<>();
    private static List<Delivery> normalDeliveryList = new ArrayList<>();

    static Products init(Context context) {
        Products allProducts = null;
        greenList = new HashMap<>();

        greenList.put("12345", Arrays.asList(currentDay, currentDay.plusDays(1), currentDay.plusDays(4)));
        greenList.put("11122", Arrays.asList(currentDay, currentDay.plusDays(1)));
        greenList.put("12344", Arrays.asList(currentDay, currentDay.plusDays(3)));


        Gson gson = new Gson();

        String json;
        try {
            InputStream is = context.getResources().openRawResource(R.raw.productslist);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            allProducts = gson.fromJson(json, Products.class);

            Log.d("init", "Prods: " + json);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.d("init", "error while tried to read file");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return allProducts;
    }


    static public void addProduct(Product product) {

        if (ProductType.EXTERNAL.name().equalsIgnoreCase(product.getProductType())) {
            startDay = currentDay.plusDays(EXTERNAL_PROD_PLUS_DAYS);
        }

        if (ProductType.TEMPORARY.name().equalsIgnoreCase(product.getProductType())) {
            lastDay = currentDay.plusDays(DateTimeConstants.SUNDAY - currentDay.getDayOfWeek());
        }

        if (currentDay.plusDays(product.getDaysInAdvance()).getMillis() > startDay.getMillis()) {
            startDay = currentDay.plusDays(product.getDaysInAdvance());
        }

        products.add(product);
        Log.d("PROD NAME", product.getName());
    }

    public static void removeProduct(Product product) {
        Log.d("PROD NAME", product.getName());
        products.remove(product);
    }

    public static void deliveryDays(Context context) {
        DateTime auxDay = startDay;
        boolean isValidDeliveryDay = true;

        while (auxDay.getMillis() < lastDay.getMillis()) {

            for (Product product : products) {
                if (!product.getDeliveryDays().contains(auxDay.dayOfWeek().getAsShortText())) {
                    isValidDeliveryDay = false;
                    break;
                } else {
                    isValidDeliveryDay = true;
                }
            }

            if (isValidDeliveryDay) {

                if (auxDay.getMillis() <= currentDay.plusDays(LIMIT_GREEN_DELIVERY_DAY).getMillis()
                        && greenList.get(postCode).contains(auxDay)) {

                    deliveryGreenList.add(new Delivery(postCode, auxDay.toString(), true));
                } else {
                    normalDeliveryList.add(new Delivery(postCode, auxDay.toString(), false));
                }
            }
            auxDay = auxDay.plusDays(1);
        }

        printDeliveryDays(context);
    }

    private static void printDeliveryDays(Context context) {
        StringBuilder result = new StringBuilder();

        if (deliveryGreenList.size() > 0 || normalDeliveryList.size() > 0) {
            for (Delivery delivery : deliveryGreenList) {
                result.append(delivery.printDelivery(context)).append("\n");
            }

            for (Delivery delivery : normalDeliveryList) {
                result.append(delivery.printDelivery(context)).append("\n");
            }

            Log.d("DELIVERY", context.getResources().getString(R.string.result_format, result));

        } else {
            Log.d("DELIVERY", context.getResources().getString(R.string.result_fail_format));
        }
    }
}
