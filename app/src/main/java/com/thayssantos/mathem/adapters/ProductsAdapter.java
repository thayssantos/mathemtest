package com.thayssantos.mathem.adapters;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thayssantos.mathem.DeliveryValidation;
import com.thayssantos.mathem.R;
import com.thayssantos.mathem.model.Product;
import com.thayssantos.mathem.model.Products;

import java.util.ArrayList;
import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {
    List<Product> products;
    List<Product> productsDelivery;

    public ProductsAdapter(Products products) {
        this.products = products.getProducts();
        productsDelivery = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int position) {
        final Product product = products.get(position);
        viewHolder.textView.setText(product.getName());

        if (position % 2 == 0) {
            viewHolder.view.setBackgroundColor(Color.argb(50,250,128,114));
        }

        if (product.isChecked()) {
            viewHolder.checkBoxView.setBackgroundResource(R.drawable.ic_checkbox_on);
        } else {
            viewHolder.checkBoxView.setBackgroundResource(R.drawable.ic_checkbox_off);
        }

        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (product.isChecked()) {
                    viewHolder.checkBoxView.setBackgroundResource(R.drawable.ic_checkbox_off);
                    product.setChecked(false);
                    productsDelivery.remove(product);
                    DeliveryValidation.removeProduct(product);

                } else {
                    viewHolder.checkBoxView.setBackgroundResource(R.drawable.ic_checkbox_on);
                    product.setChecked(true);
                    productsDelivery.add(product);
                    DeliveryValidation.addProduct(product);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;
        public final TextView textView;
        public final ImageView checkBoxView;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            textView = view.findViewById(R.id.textView);
            checkBoxView = view.findViewById(R.id.iv_checkbox);
        }
    }
}
