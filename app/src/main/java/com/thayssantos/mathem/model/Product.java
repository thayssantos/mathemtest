package com.thayssantos.mathem.model;


import java.io.Serializable;
import java.util.List;

public class Product implements Serializable{

    private String productId;
    private String name;
    private List<String> deliveryDays; //a list of weekdays when the product can be delivered
    private String productType;
    private int daysInAdvance; //how many days before delivery the products need to be ordered
    private boolean isChecked = false;

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public String getProductId() {
        return productId;
    }

    public String getName() {
        return name;
    }

    public List<String> getDeliveryDays() {
        return deliveryDays;
    }

    public String getProductType() {
        return productType;
    }

    public int getDaysInAdvance() {
        return daysInAdvance;
    }

    public String printProduct() {
        return "productId: " + productId +
                "\n name: " + name +
                "\n productType: " + productType;
    }
}
