package com.thayssantos.mathem.model;

import android.content.Context;

import com.thayssantos.mathem.R;

import org.joda.time.DateTime;

public class Delivery {

    private String postalCode;
    private String deliveryDate;
    private boolean isGreenDelivery;

    public Delivery(String postalCode, String deliveryDate, boolean isGreenDelivery) {
        this.postalCode = postalCode;
        this.deliveryDate = deliveryDate;
        this.isGreenDelivery = isGreenDelivery;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public boolean isGreenDelivery() {
        return isGreenDelivery;
    }

    public String printDelivery(Context context) {
        return String.format(context.getResources().getString(R.string.delivery_format),
                getPostalCode(),
                getDeliveryDate(),
                isGreenDelivery());
    }
}
