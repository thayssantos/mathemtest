package com.thayssantos.mathem.model.constants;

public enum ProductType {
    NORMAL,
    EXTERNAL,
    TEMPORARY
}
